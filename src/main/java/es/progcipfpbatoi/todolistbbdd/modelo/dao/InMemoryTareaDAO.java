package es.progcipfpbatoi.todolistbbdd.modelo.dao;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import es.progcipfpbatoi.todolistbbdd.excepciones.AlreadyExistsException;
import es.progcipfpbatoi.todolistbbdd.excepciones.NotFoundException;
import es.progcipfpbatoi.todolistbbdd.modelo.dao.interfaces.TareaDao;
import es.progcipfpbatoi.todolistbbdd.modelo.entidades.Prioridad;
import es.progcipfpbatoi.todolistbbdd.modelo.entidades.Tarea;

@Repository
public class InMemoryTareaDAO implements TareaDao{

	private ArrayList<Tarea> tareas;
	
	public InMemoryTareaDAO() {
		this.tareas = new ArrayList<>();
		init();
	}
	
	@Override
	public ArrayList<Tarea> findAll() {
		return tareas;
	}

	@Override
	public ArrayList<Tarea> findAllWithParams(Boolean isRealizada, LocalDate fecha, String usuario) {
		ArrayList<Tarea> tareasFiltradas = new ArrayList<>();
		
		for (Tarea tarea : tareas) {

			boolean cumpleIsRealizada = isRealizada == null || tarea.isRealizado() == isRealizada;
			boolean cumpleFecha = fecha == null || tarea.getCreadoEn().equals(fecha);
			boolean cumpleUsuario = usuario == null || tarea.getUsuario().equals(usuario);
			
			if (cumpleIsRealizada && cumpleFecha && cumpleUsuario) {
				tareasFiltradas.add(tarea);
			}
		}
		
		return tareasFiltradas;
		
	}

	@Override
	public ArrayList<Tarea> findAllWithParams(String usuario) {
		ArrayList<Tarea> tareasFiltradas = new ArrayList<>();
        for (Tarea tarea: tareas) {
            if (tarea.perteneceAUsuario(usuario)) {
                tareasFiltradas.add(tarea);
            }
        }
        return tareasFiltradas;
	}

	@Override
	public Tarea findById(int id) {
		Tarea tarea = new Tarea(id);
        if (tareas.contains(tarea)){
            return tareas.get(tareas.indexOf(tarea));
        }
        return null;
	}

	@Override
	public Tarea getById(int id) throws NotFoundException {
		Tarea tarea = findById(id);
        if (tarea == null) {
            throw new NotFoundException("La tarea con codigo " + id + " no existe");
        }
        return tarea;
	}

	@Override
	public void add(Tarea tarea) throws AlreadyExistsException{
		if (!tareas.contains(tarea)) {
    		this.tareas.add(tarea);
    	}
    	
    	throw new AlreadyExistsException(tarea);
	}

	@Override
	public void update(Tarea tarea) throws NotFoundException {
		
		int indiceTarea = tareas.indexOf(tarea);
		
		if (indiceTarea == -1) {
			throw new NotFoundException("La tarea " + tarea + " no existe");
		}
		
		Tarea tareaAActualizar = tareas.get(indiceTarea);
		tareaAActualizar.set(tarea);
	}

	@Override
	public void delete(Tarea tarea) throws NotFoundException{
		if (!tareas.contains(tarea)) {
			throw new NotFoundException("La tarea " + tarea + " no existe");
		}
		
		tareas.remove(tarea);
	}
	
	private void init() {
        this.tareas.add(new Tarea(tareas.size(), "Roberto", "Hacer la cama",  Prioridad.MEDIA, LocalDate.of(2024, Month.FEBRUARY, 2), LocalTime.of(8, 30, 12), true));
        this.tareas.add(new Tarea(tareas.size(), "Sergio", "Hacer la comida", Prioridad.ALTA, LocalDate.of(2024, Month.FEBRUARY, 8), LocalTime.of(12, 30, 12), true));
        this.tareas.add(new Tarea(tareas.size(), "Raúl", "ir a comprar",  Prioridad.MEDIA, LocalDate.of(2024, Month.FEBRUARY, 12), LocalTime.of(17, 30, 12), true));
        this.tareas.add(new Tarea(tareas.size(), "Sergio", "Estudiar",  Prioridad.BAJA, LocalDate.of(2024, Month.FEBRUARY, 22), LocalTime.of(8, 30, 12), true));
    }


}
