package es.progcipfpbatoi.todolistbbdd.modelo.entidades;

import java.util.InputMismatchException;

public enum Prioridad {

    ALTA, BAJA, MEDIA;

    public static Prioridad fromText(String value){
        if (value.equals("ALTA")) {
            return Prioridad.ALTA;
        } else  if (value.equals("BAJA")) {
            return Prioridad.BAJA;
        } else  if (value.equals("MEDIA")) {
            return Prioridad.MEDIA;
        }
        throw new InputMismatchException("El valor " + value + " es inválido");
    }
}